﻿using System;
using System.Collections.Generic;
using System.Linq;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RTP.CP;
using TcpNtcpCalculation.Immutables;

namespace RadiationTreatmentPlanner.Optimization
{
    public class RtpOptimizationRunResult
    {
        public string PatientId { get; }
        public string CourseId { get; }
        public string PlanSetupId { get; }
        public string StructureSetId { get; }
        public string Description { get; }
        public Option<UDose> TotalPrescriptionDose { get; }
        public uint NumberOfFractions { get; }
        public ProbabilityOfCureCalculationResult ProbabilityOfCureCalculationResult { get; }
        public ProbabilityOfInjuryCalculationResult ProbabilityOfInjuryCalculationResult { get; }
        public ComplicationFreeTumorControl ComplicationFreeTumorControl { get; }
        public Option<ClinicalProtocolEvaluationResult> ClinicalProtocolEvaluationResult { get; }
        public DateTime Timestamp { get; }
        public object OptionalAdditionalInformation { get; }
        private uint _numberOfFloatingPointDigits = 3u;

        /// <summary>
        /// Optimization result of one radiation treatment plan optimization.
        /// </summary>
        /// <param name="patientId">Patient ID.</param>
        /// <param name="courseId">Course ID.</param>
        /// <param name="planSetupId">Plan Setup ID.</param>
        /// <param name="structureSetId">Structure Set ID.</param>
        /// <param name="description">Description of optimization.</param>
        /// <param name="totalPrescriptionDose">Total prescription dose (optional).</param>
        /// <param name="numberOfFractions">Number of fractions.</param>
        /// <param name="probabilityOfCureCalculationResult">Probability of cure calculation result.</param>
        /// <param name="probabilityOfInjuryCalculationResult">Probability of injury calculation result.</param>
        /// <param name="complicationFreeTumorControl">Complication free tumor control.</param>
        /// <param name="clinicalProtocolEvaluationResult">Clinical Protocol evaluation result (optional)</param>
        /// <param name="timestamp">Timestamp of optimization.</param>
        /// <param name="optionalAdditionalInformation">Optional additional information.</param>
        public RtpOptimizationRunResult(string patientId, string courseId, string planSetupId, string structureSetId,
            string description, Option<UDose> totalPrescriptionDose, uint numberOfFractions,
            ProbabilityOfCureCalculationResult probabilityOfCureCalculationResult,
            ProbabilityOfInjuryCalculationResult probabilityOfInjuryCalculationResult,
            ComplicationFreeTumorControl complicationFreeTumorControl,
            Option<ClinicalProtocolEvaluationResult> clinicalProtocolEvaluationResult,
            DateTime timestamp,
            object optionalAdditionalInformation)
        {
            PatientId = patientId ?? throw new ArgumentNullException(nameof(patientId));
            CourseId = courseId ?? throw new ArgumentNullException(nameof(courseId));
            PlanSetupId = planSetupId ?? throw new ArgumentNullException(nameof(planSetupId));
            StructureSetId = structureSetId ?? throw new ArgumentNullException(nameof(structureSetId));
            Description = description ?? throw new ArgumentNullException(nameof(description));
            TotalPrescriptionDose = totalPrescriptionDose;
            NumberOfFractions = numberOfFractions;
            ProbabilityOfCureCalculationResult = probabilityOfCureCalculationResult ??
                                                 throw new ArgumentNullException(
                                                     nameof(probabilityOfCureCalculationResult));
            ProbabilityOfInjuryCalculationResult = probabilityOfInjuryCalculationResult ??
                                                   throw new ArgumentNullException(
                                                       nameof(probabilityOfInjuryCalculationResult));
            ComplicationFreeTumorControl = complicationFreeTumorControl;
            ClinicalProtocolEvaluationResult = clinicalProtocolEvaluationResult;
            Timestamp = timestamp;
            OptionalAdditionalInformation = optionalAdditionalInformation;
        }

        public override string ToString()
        {
            string msg = $"Patient ID:\t{PatientId}\n" +
                         $"Plan ID:\t{PlanSetupId}\n" +
                         $"Course ID:\t{CourseId}\n" +
                         $"Description:\t{Description}\n" +
                         $"Total Prescription Dose: \t" +
                         $"{TotalPrescriptionDose.ValueOr(new UDose(double.NaN, UDose.UDoseUnit.Unknown)).ToString((uint)_numberOfFloatingPointDigits)}\n" +
                         $"Number of fractions:\t{NumberOfFractions}\n";

            AddProbabilityOfCureResults(msg);
            msg = AddProbabilityOfCureResults(msg);
            msg = AddProbabilityOfInjuryResults(msg);
            msg = AddComplicationFreeControl(msg);
            if (ClinicalProtocolEvaluationResult.HasValue)
            {
                var clinicalProtocolEvaluationResult =
                    ClinicalProtocolEvaluationResult.ValueOr(
                        new ClinicalProtocolEvaluationResult(Array.Empty<ClinicalProtocolItemEvaluationResult>()));
                msg = AddClinicalConstraints(msg, clinicalProtocolEvaluationResult);
            }
            return AddOptionalAdditionalInfo(msg);
        }

        private string AddOptionalAdditionalInfo(string msg)
        {
            if (OptionalAdditionalInformation != null) msg += OptionalAdditionalInformation.ToString();
            return msg;
        }

        private string AddComplicationFreeControl(string msg)
        {
            msg += $"P+: \t{Round(ComplicationFreeTumorControl.Value)}\n";
            return msg;
        }

        private string AddProbabilityOfCureResults(string msg)
        {
            var probabilityOfCureValue = Round(ProbabilityOfCureCalculationResult.ProbabilityOfCure.Value);
            msg += $"Probability of cure: \t{probabilityOfCureValue}\n";
            foreach (var tcpPoint in ProbabilityOfCureCalculationResult.TcpPoints)
                msg += $"\tTCP_{tcpPoint.StructureId}: \t{Round(tcpPoint.TumorControlProbability.Value)}\n";
            return msg;
        }

        private string AddProbabilityOfInjuryResults(string msg)
        {
            var probabilityOfInjuryValue = Round(ProbabilityOfInjuryCalculationResult.ProbabilityOfInjury.Value);
            msg += $"Probability of injury: \t{probabilityOfInjuryValue}\n";
            foreach (var tcpPoint in ProbabilityOfInjuryCalculationResult.NtcpPoints)
                msg += $"\tNTCP_{tcpPoint.StructureId}: \t{Round(tcpPoint.NormalTissueComplicationProbability.Value)}\n";
            return msg;
        }

        private string AddClinicalConstraints(string msg,
            ClinicalProtocolEvaluationResult clinicalProtocolEvaluationResult)
        {
            msg = AddHardConstraints(msg, clinicalProtocolEvaluationResult);
            return AddSoftConstraints(msg, clinicalProtocolEvaluationResult);
        }

        private string AddSoftConstraints(string msg, ClinicalProtocolEvaluationResult clinicalProtocolEvaluationResult)
        {
            if (clinicalProtocolEvaluationResult.AreAllSoftConstraintsSatisfied)
            {
                msg += "All soft constraints are satisfied.\n";
                return msg;
            }

            msg += "Following soft constraints are not satisfied:\n";

            var dict = GetDictionaryOfNotSatisfiedSoftConstraintsWithRtRoiTypeAsKey(clinicalProtocolEvaluationResult);

            foreach (var rtRoiTypeCode in dict.Keys)
            {
                msg += $"- {rtRoiTypeCode}:\n";
                var evaluationResults = dict[rtRoiTypeCode];
                foreach (var evaluationResult in evaluationResults)
                {
                    var clinicalProtocolItem = evaluationResult.ClinicalProtocolItem;
                    if (IsMaximumConstraint(clinicalProtocolItem))
                        msg +=
                            $"\tMaximum - target: {clinicalProtocolItem.Dose.ToString(_numberOfFloatingPointDigits)}, " +
                            $"actual: {evaluationResult.ActualDose.ToString(_numberOfFloatingPointDigits)}\n";
                    else if (IsMinimumConstraint(clinicalProtocolItem))
                        msg +=
                            $"\tMinimum - target: {clinicalProtocolItem.Dose.ToString(_numberOfFloatingPointDigits)}, " +
                            $"actual: {evaluationResult.ActualDose.ToString(_numberOfFloatingPointDigits)}\n";
                }
            }

            return msg;
        }

        private Dictionary<string, List<ClinicalProtocolItemEvaluationResult>>
            GetDictionaryOfNotSatisfiedSoftConstraintsWithRtRoiTypeAsKey(
                ClinicalProtocolEvaluationResult clinicalProtocolEvaluationResult)
        {
            var notSatisfiedHardConstraints = clinicalProtocolEvaluationResult
                .ClinicalProtocolItemEvaluationResults
                .Where(x => !x.IsClinicalProtocolSatisfied &&
                            x.ClinicalProtocolItem.ConstraintType == ConstraintType.Soft);

            var dict = new Dictionary<string, List<ClinicalProtocolItemEvaluationResult>>();

            foreach (var evaluationResult in notSatisfiedHardConstraints)
            {
                var rtRoiTypeCode = evaluationResult.ClinicalProtocolItem.RegionOfInterestType.Code;
                if (dict.ContainsKey(rtRoiTypeCode)) dict[rtRoiTypeCode].Add(evaluationResult);
                else
                    dict.Add(rtRoiTypeCode, new List<ClinicalProtocolItemEvaluationResult> { evaluationResult });
            }

            return dict;
        }

        private string AddHardConstraints(string msg, ClinicalProtocolEvaluationResult clinicalProtocolEvaluationResult)
        {
            if (clinicalProtocolEvaluationResult.AreAllHardConstraintsSatisfied)
            {
                msg += "All hard constraints are satisfied.\n";
                return msg;
            }

            msg += "Following hard constraints are not satisfied:\n";

            var dict = GetDictionaryOfNotSatisfiedHardConstraintsWithRtRoiTypeAsKey(clinicalProtocolEvaluationResult);

            foreach (var rtRoiTypeCode in dict.Keys)
            {
                msg += $"- {rtRoiTypeCode}:\n";
                var evaluationResults = dict[rtRoiTypeCode];
                foreach (var evaluationResult in evaluationResults)
                {
                    var clinicalProtocolItem = evaluationResult.ClinicalProtocolItem;
                    if (IsMaximumConstraint(clinicalProtocolItem))
                        msg +=
                            $"\tMaximum - target: {clinicalProtocolItem.Dose.ToString(_numberOfFloatingPointDigits)}, " +
                            $"actual: {evaluationResult.ActualDose.ToString(_numberOfFloatingPointDigits)}\n";
                    else if (IsMinimumConstraint(clinicalProtocolItem))
                        msg +=
                            $"\tMinimum - target: {clinicalProtocolItem.Dose.ToString(_numberOfFloatingPointDigits)}, " +
                            $"actual: {evaluationResult.ActualDose.ToString(_numberOfFloatingPointDigits)}\n";
                }
            }

            return msg;
        }

        private Dictionary<string, List<ClinicalProtocolItemEvaluationResult>>
            GetDictionaryOfNotSatisfiedHardConstraintsWithRtRoiTypeAsKey(
                ClinicalProtocolEvaluationResult clinicalProtocolEvaluationResult)
        {
            var notSatisfiedHardConstraints = clinicalProtocolEvaluationResult
                .ClinicalProtocolItemEvaluationResults
                .Where(x => !x.IsClinicalProtocolSatisfied &&
                            x.ClinicalProtocolItem.ConstraintType == ConstraintType.Hard);

            var dict = new Dictionary<string, List<ClinicalProtocolItemEvaluationResult>>();

            foreach (var evaluationResult in notSatisfiedHardConstraints)
            {
                var rtRoiTypeCode = evaluationResult.ClinicalProtocolItem.RegionOfInterestType.Code;
                if (dict.ContainsKey(rtRoiTypeCode)) dict[rtRoiTypeCode].Add(evaluationResult);
                else
                    dict.Add(rtRoiTypeCode, new List<ClinicalProtocolItemEvaluationResult> { evaluationResult });
            }

            return dict;
        }

        private bool IsMaximumConstraint(IClinicalProtocolItem item)
        {
            return item is MaximumClinicalProtocolItem || item is MaximumDoseClinicalProtocolItem;
        }
        private bool IsMinimumConstraint(IClinicalProtocolItem item)
        {
            return item is MinimumClinicalProtocolItem || item is MinimumDoseClinicalProtocolItem;
        }

        private double Round(double number) => Math.Round(number, (int)_numberOfFloatingPointDigits);


        public string ToString(uint numberOfFloatingPointDigits)
        {
            _numberOfFloatingPointDigits = numberOfFloatingPointDigits;
            return ToString();
        }

        protected bool Equals(RtpOptimizationRunResult other)
        {
            return _numberOfFloatingPointDigits == other._numberOfFloatingPointDigits && PatientId == other.PatientId &&
                   CourseId == other.CourseId && PlanSetupId == other.PlanSetupId &&
                   StructureSetId == other.StructureSetId && Description == other.Description &&
                   TotalPrescriptionDose.Equals(other.TotalPrescriptionDose) &&
                   NumberOfFractions == other.NumberOfFractions &&
                   Equals(ProbabilityOfCureCalculationResult, other.ProbabilityOfCureCalculationResult) &&
                   Equals(ProbabilityOfInjuryCalculationResult, other.ProbabilityOfInjuryCalculationResult) &&
                   ComplicationFreeTumorControl.Equals(other.ComplicationFreeTumorControl) &&
                   ClinicalProtocolEvaluationResult.Equals(other.ClinicalProtocolEvaluationResult) &&
                   Timestamp.Equals(other.Timestamp) &&
                   Equals(OptionalAdditionalInformation, other.OptionalAdditionalInformation);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RtpOptimizationRunResult)obj);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(_numberOfFloatingPointDigits);
            hashCode.Add(PatientId);
            hashCode.Add(CourseId);
            hashCode.Add(PlanSetupId);
            hashCode.Add(StructureSetId);
            hashCode.Add(Description);
            hashCode.Add(TotalPrescriptionDose);
            hashCode.Add(NumberOfFractions);
            hashCode.Add(ProbabilityOfCureCalculationResult);
            hashCode.Add(ProbabilityOfInjuryCalculationResult);
            hashCode.Add(ComplicationFreeTumorControl);
            hashCode.Add(ClinicalProtocolEvaluationResult);
            hashCode.Add(Timestamp);
            hashCode.Add(OptionalAdditionalInformation);
            return hashCode.ToHashCode();
        }
    }
}
